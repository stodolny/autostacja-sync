import pyodbc


class QueryResultTransformer:

    @staticmethod
    def transform(results: pyodbc.Cursor):
        transformed = []
        for row in results:
            transformed.append(dict(zip([t[0] for t in row.cursor_description], row)))

        return transformed
