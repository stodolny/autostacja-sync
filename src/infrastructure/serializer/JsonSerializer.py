import simplejson as json
from datetime import date, datetime


class JsonSerializer:
    @staticmethod
    def __json_serial(obj):
        """JSON serializer for objects not serializable by default json code"""
        if isinstance(obj, bytes):
            return int.from_bytes(obj, byteorder='big')
        if isinstance(obj, (datetime, date)):
            return obj.isoformat()
        raise TypeError("Type %s not serializable" % type(obj))

    @staticmethod
    def serialize(data):
        return json.dumps(data, encoding=None, default=JsonSerializer.__json_serial, indent=4)