class LocalData:
    ignoreOrderIds = []

    def __init__(self, data: dict):
        if "ignoreOrderIds" in data:
            self.ignoreOrderIds = data.ignoreOrderIds;

    def asDict(self) -> dict:
        return {
            'ignoreOrderIds': self.ignoreOrderIds
        }