from src.domain.query.BaseQuery import Query


class GetAllScenarioStagesQuery(Query):
    def execute(self):
        cur = self._getCursor()
        results = cur.execute("select * from ModelDanychContainer.EtapyScenariuszaObslugiKlienta").fetchall()
        cur.close()
        return self._transformResults(results)