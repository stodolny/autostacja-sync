import pyodbc
from src.infrastructure.database.QueryResultTransformer import QueryResultTransformer


class Query:
    conn: pyodbc.Connection

    def __init__(self, conn: pyodbc.Connection):
        self.conn = conn

    def _getCursor(self):
        return self.conn.cursor()

    @staticmethod
    def _transformResults(results):
        return QueryResultTransformer.transform(results=results)
