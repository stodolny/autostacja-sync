from src.domain.query.BaseQuery import Query


class GetAllScenariosQuery(Query):
    def execute(self):
        cur = self._getCursor()
        results = cur.execute("select * from ModelDanychContainer.ScenariuszeObslugiKlienta").fetchall()
        cur.close()
        return self._transformResults(results)