from typing import TypeVar, Type
import pyodbc

T = TypeVar('T')


class QueryFactory:
    conn: pyodbc.Connection

    def __init__(self, conn: pyodbc.Connection):
        self.conn = conn

    def create(self, queryClass: Type[T]) -> T:
        return queryClass(self.conn)
