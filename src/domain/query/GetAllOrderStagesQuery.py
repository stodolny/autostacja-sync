from src.domain.query.BaseQuery import Query


class GetAllOrderStagesQuery(Query):
    def execute(self):
        cur = self._getCursor()
        results = cur.execute("select * from ModelDanychContainer.EtapyZleceniaSerwisowego").fetchall()
        cur.close()
        return self._transformResults(results)