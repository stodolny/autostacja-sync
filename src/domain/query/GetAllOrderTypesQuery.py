from src.domain.query.BaseQuery import Query


class GetAllOrderTypesQuery(Query):
    def execute(self):
        cur = self._getCursor()
        results = cur.execute("select * from ModelDanychContainer.RodzajeZlecenSerwisowych").fetchall()
        cur.close()
        return self._transformResults(results)