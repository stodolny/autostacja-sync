from src.domain.query.BaseQuery import Query


class GetAllOrdersQuery(Query):
    def execute(self):
        cur = self._getCursor()
        results = cur.execute("select * from ModelDanychContainer.ZleceniaSerwisowe").fetchall()
        cur.close()
        return self._transformResults(results)