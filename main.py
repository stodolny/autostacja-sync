from datetime import datetime

import pyodbc
import os
import shutil
import requests
from sys import platform
from dotenv import load_dotenv
from pprint import pformat
import simplejson as json

from src.domain.model.LocalData import LocalData
from src.domain.query.GetAllOrderStagesQuery import GetAllOrderStagesQuery
from src.domain.query.GetAllOrderTypesQuery import GetAllOrderTypesQuery
from src.domain.query.GetAllOrdersQuery import GetAllOrdersQuery
from src.domain.query.GetAllScenarioStagesQuery import GetAllScenarioStagesQuery
from src.domain.query.GetAllScenariosQuery import GetAllScenariosQuery
from src.domain.query.QueryFactory import QueryFactory
from src.infrastructure.serializer.JsonSerializer import JsonSerializer


def checkFilePermissions():
    time = datetime.now().strftime('U')

    with open('./var/checkPermissions', 'w+') as f:
        f.write(time)
        f.close()

    with open('./var/checkPermissions', 'r') as f:
        if f.read() != time:
            f.close()
            raise RuntimeError("./var is not writable")

        f.close()
        os.remove('./var/checkPermissions')


if __name__ == '__main__':
    load_dotenv(verbose=True)
    checkFilePermissions()

    with open('./var/localData.json', 'w+') as f:
        data = f.read()
        localData = LocalData(json.loads(data) if data else {})
        f.close()

    localData.ignoreOrderIds.append(1);

    driver = os.getenv('DB_DRIVER')
    server = os.getenv('DB_SERVER')
    instance = os.getenv('DB_INSTANCE')
    database = os.getenv('DB_DATABASE')
    user = os.getenv('DB_USER')
    password = os.getenv('DB_PASSWORD')
    port = os.getenv('DB_PORT')

    if platform == "darwin":
        connectionString = f'DSN=AUTOSTACJA;UID={user};PWD={password};DATABASE={database};ApplicationIntent=ReadOnly;readonly=TRUE;unicode_results=True'
    else:
        connectionString = f'DRIVER={{{driver}}};SERVER={server};DATABASE={database};UID={user};PWD={password};ApplicationIntent=ReadOnly;readonly=TRUE;unicode_results=True'

    conn = pyodbc.connect(connectionString, unicode_results=True, readonly=True)
    conn.setdecoding(pyodbc.SQL_CHAR, encoding='utf8')
    conn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf8')
    conn.setencoding(encoding='utf8')

    queryFactory = QueryFactory(conn)

    payload = {}
    ordersQuery = queryFactory.create(queryClass=GetAllOrdersQuery)
    payload['orders'] = ordersQuery.execute()

    orderStagesQuery = queryFactory.create(queryClass=GetAllOrderStagesQuery)
    payload['orderStages'] = orderStagesQuery.execute()

    orderTypesQuery = queryFactory.create(queryClass=GetAllOrderTypesQuery)
    payload['orderTypes'] = orderTypesQuery.execute()

    scenariosQuery = queryFactory.create(queryClass=GetAllScenariosQuery)
    payload['scenarios'] = scenariosQuery.execute()

    scenariosStagesQuery = queryFactory.create(queryClass=GetAllScenarioStagesQuery)
    payload['scenarioStages'] = scenariosStagesQuery.execute()

    print(pformat(JsonSerializer.serialize(payload)))

    conn.close()

    shutil.copy('./var/localData.json', './var/localData.json.backup')
    with open('./var/localData.json', 'w+') as f:
        f.write(json.dumps(localData.asDict()))
        f.close()

    r = requests.get('https://api.github.com/events')
    print(r)

